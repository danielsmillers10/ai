import pygame

class Button():
    def __init__(self, window, x, y, image):
        self.image = image
        self.rect = self.image.get_rect()
        self.rect.topleft = (x, y)
        self.clicked = False
        self.window = window

    def draw(self):
        action = False
        mouse = pygame.mouse.get_pos()

        if self.rect.collidepoint(mouse):
            if pygame.mouse.get_pressed()[0] == 1 and self.clicked == False:
                self.clicked = True
                action = True
                print("clicked")

        if pygame.mouse.get_pressed()[0] == 0:
            self.clicked = False

        self.window.blit(self.image, (self.rect.x, self.rect.y))

        return action