import pygame
import os
import button

pygame.init()

clock = pygame.time.Clock()
fps = 60
clicked = False
direction = ''
sum_stage_points = 0

green = (0, 255, 0)
red = (255, 0, 0)
blue = (0, 0, 255)

font = pygame.font.SysFont('Comic Sans', 26)

width, height = 1000, 400
scr = pygame.display.set_mode((width, height))

level_img = pygame.image.load(os.path.join('Assets', 'Level.png'))

arrow_up_img = pygame.image.load(os.path.join('Assets', 'ArrowUp.png'))
arrow_up_img_trans = pygame.transform.scale(arrow_up_img, (50, 50))

arrow_down_img = pygame.image.load(os.path.join('Assets', 'ArrowDown.png'))
arrow_down_img_trans = pygame.transform.scale(arrow_down_img, (50, 50))

red_win_img = pygame.image.load(os.path.join('Assets', 'RedWin.png'))
blue_win_img = pygame.image.load(os.path.join('Assets', 'BlueWin.png'))
tie_img = pygame.image.load(os.path.join('Assets', 'Tie.png'))

def draw_text(text, font, text_col, x, y):
	img = font.render(text, True, text_col)
	scr.blit(img, (x, y))

def draw_level():
    scr.blit(level_img, (0, 0))
    draw_text(f'Red points: {player_one.points}', font, red, 50, 300)
    draw_text(f'Blue points: {player_two.points}', font, blue, 250, 300)



class Player():
    def __init__(self, x, y, name):
        self.name = name
        img = pygame.image.load(f'Assets/{self.name}.png')
        self.image = pygame.transform.scale(img, (img.get_width() * 0.4, img.get_height() * 0.4))
        self.rect = self.image.get_rect()
        self.rect.center = (x, y)
        self.stage = 0
        self.points = 0
        self.turn = True
    def draw(self):
        scr.blit(self.image, self.rect)


class Points():
    def __init__(self, x, y, points):
        self.x = x
        self.y = y
        self.points = points
    def draw(self, points):
        self.points = points

    
def stage_nodes(stage):
    if stage == 1:
        one = [0,-1]
        return one
    elif stage == 2:
        two = [0,1,-1,0]
        return two
    elif stage == 3:
        three = [0,0,1,1,-1,-1,0,0]
        return three
    elif stage == 4:
        four = [0,0,0,0,1,1,1,1,-1,-1,-1,-1,0,0,0,0]
        return four
    elif stage == 5:
        five = [0,-1,0,-1,0,-1,0,-1,1,0,1,0,1,0,1,0,-1,-1,-1,-1,-1,-1,-1,-1,0,-1,0,-1,0,-1,0,-1]
        return five
    elif stage == 6:
        six = [0,1,-1,0,0,1,-1,0,0,1,-1,0,0,1,-1,0,1,1,0,1,1,1,0,1,1,1,0,1,1,1,0,1,-1,0,-1,-1,-1,0,-1,-1,-1,0,-1,-1,-1,0,-1,-1,0,1,-1,0,0,1,-1,0,0,1,-1,0,0,1,-1,0]
        return six

def stage_points(stage, direction):
    if stage == 1 and direction == 'up':
        return 2
    if stage == 1 and direction == 'down':
        return 1
    if stage == 2 and direction == 'up':
        return -1
    if stage == 2 and direction == 'down':
        return 1
    if stage == 3 and direction == 'up':
        return -2
    if stage == 3 and direction == 'down':
        return 1

player_one = Player(90, 82, 'Red')
player_two = Player(90, 215, 'Blue')

arrow_up = button.Button(scr, 900, 280, arrow_up_img_trans)
arrow_down = button.Button(scr, 900, 340, arrow_down_img_trans)

player_one_points = Points(0, 0, player_one.points)


def main():

    turn_nr = 2
    run = True
    while run:
        clock.tick(fps)
        
        draw_level()

        player_one.draw()
        player_two.draw()

        arrow_up.draw()
        arrow_down.draw()

        player_one_points.draw(player_one.points)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
            
            if player_one.turn:
                if arrow_up.draw():
                    direction = 'up'
                    player_one.rect.x += 115
                    player_one.stage += 1
                    if stage_points(player_one.stage, direction) < 0:
                        player_two.points += stage_points(player_one.stage, direction)
                        player_one.turn = False
                    elif stage_points(player_one.stage, direction) > 0:
                        player_one.points += stage_points(player_one.stage, direction)
                        player_one.turn = False
                    

                if arrow_down.draw():
                    direction = 'down'
                    player_one.rect.x += 115
                    player_one.stage += 1
                    if stage_points(player_one.stage, direction) < 0:
                        player_two.points += stage_points(player_one.stage, direction)
                        player_one.turn = False
                    elif stage_points(player_one.stage, direction) > 0:
                        player_one.points += stage_points(player_one.stage, direction)
                        player_one.turn = False
                
            if player_one.turn == False:
                
                for i in range(len(stage_nodes(turn_nr))):
                    if stage_nodes(turn_nr)[i] == 0: 
                            max_value = stage_nodes(turn_nr)[i]
                    elif stage_nodes(turn_nr)[i] == 1: 
                            max_value = stage_nodes(turn_nr)[i]
                    elif stage_nodes(turn_nr)[i] == -1: 
                            max_value = stage_nodes(turn_nr)[i]
                        
                if stage_nodes(turn_nr).index(max_value) % 2 == 0:
                    direction = 'up'
                    player_two.rect.x += 115
                    player_two.stage += 1
                    if stage_points(player_two.stage, direction) < 0:
                        player_one.points += stage_points(player_two.stage, direction)
                    elif stage_points(player_two.stage, direction) > 0:
                        player_two.points += stage_points(player_two.stage, direction)

                if stage_nodes(turn_nr).index(max_value) % 2 != 0:
                    direction = 'down'
                    player_two.rect.x += 115
                    player_two.stage += 1
                    if stage_points(player_two.stage, direction) < 0:
                        player_one.points += stage_points(player_two.stage, direction)
                    elif stage_points(player_two.stage, direction) > 0:
                        player_two.points += stage_points(player_two.stage, direction)

                turn_nr += 2
                player_one.turn = True

            if player_one.stage == 3 and player_two.stage == 3:
                if player_one.points > player_two.points:
                    scr.blit(red_win_img, (0, 0))
                    run = False

                elif player_one.points < player_two.points:
                    scr.blit(blue_win_img, (0, 0))
                    run = False
                    
                elif player_one.points == player_two.points:
                    scr.blit(tie_img, (0, 0))
                    run = False
                

        pygame.display.update()
        
    pygame.quit()

if __name__ == "__main__":
    main()